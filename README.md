# AdmobDemo

#### 介绍
cocos creator 快速接入admob插件
仿照官方得facebook 接入插件改写

#### 软件架构
1.暂时只支持android平台接入
20190428
[add] 支持ios平台接入

#### 安装教程

1.将根目录下packages/admob 拷贝到自己项目 packages目录下即可

#### 使用说明
一.调试代码<br>
 **Android平台** 
1. 因为google admob广告调试时需要设置TestDevice，该参数需要连接上测试得设备调用一次广告才能在日志里看到。
  将packages/admob/libs/android/AdmobHelper.java下得（可以搜索addTestDevice）
  `this.mRewardedVideoAd.loadAd(placementId,new AdRequest.Builder().addTestDevice("AD03450E17D50EFB0BBA4D19DC0EC370").build());` <br>
改为<br>
`this.mRewardedVideoAd.loadAd(placementId,new AdRequest.Builder().build());`
1. 打开cocos creator 项目, 项目---构建发布---发布平台android ----构建 ---编译 -- 运行
  建议构建发布前先清理下构建目录，可以直接删除项目根目录下得build/jsb-link文件夹
  在构建步骤结束后可以在日志上看到 类似 Admob-->  _copyFsupportFile end得日志输出就说明 插件运行正常，如果此步骤出现问题可以研究下后续得说明
1. 真机运行后可以使用android studio ,eclipse,或者直接cmd adb logcat 查看设备日志，找到addTestDevice('xxxxxxxx');这一行，可以直接搜addTestDevice
  将拷贝得测试设备码替换1.中对应得值
  `this.mRewardedVideoAd.loadAd(placementId,new AdRequest.Builder().addTestDevice("xxxxxx").build());`
1. 正式环境记得去掉testDevice设置否则广告都是测试模式 <br>

**ios平台** <br>
1. ios平台也需要设置TestDevice，脚本代码里已经添加在AdmobHelper.mm文件中，无需手动修改
1. 打开cocos creator 项目, 项目---构建发布---发布平台ios ----构建 ---编译 -- 运行
  建议构建发布前先清理下构建目录，可以直接删除项目根目录下得build/jsb-link文件夹
  在构建步骤结束后可以在日志上看到 类似 Admob-->  _copyFsupportFile end得日志输出就说明 插件运行正常
1. xcode项目是使用cocoaPods接入 admob的第一次使用的时候需要更新pod仓库会消耗一定的时间，注意查看编译日志看pod更新是否正常，一般在build/jsb-link/frameworks/runtime-src/proj.ios_mac/目录下生成了xxxx.xcworkspace工程文件的话代表编译成功
1. 使用双击或者使用xcode打开xxxx.xcworkspace工程文件，注意不要使用xxx.xcodeproj
1. 编译运行
1. 正式环境的时候需要修改Info.plist中的 GADApplicationIdentifier参数为自己应用的app_id
1. 正式环境需要删除testDevice的设置<br>

二.脚本说明
- 插件脚本得入口文件在packages/admob/main.js目录下<br>

```
module.exports = {
    load() {
        Editor.Builder.on('before-change-files', handleEvent);
    },

    unload() {
        Editor.Builder.removeListener('before-change-files', handleEvent);
    },

    messages: {}
};
```
- creator加载插件时调用load函数
- 在load函数中监听构建结束后修改文件前事件调用 handleEvent
- handleEvent中根据平台调用_handleAndroid(options) 和 _handlerIos(options);


#### 待完善功能

- 根据配置是否开启插件功能
- 对于app_id的设置还不够自动化需要手动修改
- debug模式和release模式的testDevice代码也需要手动修改
有高手会的可以帮忙完善下或者提供下思路
 


